from keras.models import Sequential
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras import backend
import tensorflow as tf
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
backend.set_image_dim_ordering('th')
tf.logging.set_verbosity(tf.logging.ERROR)


def run(train_x, train_y, test_x):
    print("neural network process starting...")
    model = Sequential()
    model.add(Conv2D(50, (5, 5), activation='relu', input_shape=(1, 28, 28), data_format='channels_first'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))
    model.add(Conv2D(20, kernel_size=3, activation='relu'))
    # max pooling layer with a window of 2 by 2
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))
    model.add(Conv2D(10, kernel_size=2, activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(60, activation='relu'))
    model.add(Dense(2, activation='softmax'))
    model.compile(metrics=['accuracy'], optimizer='RMSprop', loss='categorical_crossentropy')
    model.fit(train_x, train_y, epochs=10, batch_size=100)
    result = model.predict(test_x)
    print("neural network process finished")
    return result
