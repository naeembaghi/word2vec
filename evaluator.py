import pandas as pd
import random_forest as rf
import ada_boost as ab
import decision_tree as dt
import logistic_regression as lr
import neural_network as nn


def logistic_regression(train_x, train_y, test_x, test_ids):
    output = lr.run(train_x, train_y, test_x)
    create_submission(test_ids, output, 'logistic_regression')


def decision_tree(train_x, train_y, test_x, test_ids):
    output = dt.run(train_x, train_y, test_x)
    create_submission(test_ids, output, 'decision_tree')


def ada_boost(train_x, train_y, test_x, test_ids):
    output = ab.run(train_x, train_y, test_x)
    create_submission(test_ids, output, 'ada_boost')


def random_forest(train_x, train_y, test_x, test_ids):
    output = rf.run(train_x, train_y, test_x)
    create_submission(test_ids, output, 'random_forest')


def neural_network(train_x, train_y, test_x, test_ids):
    output = nn.run(train_x, train_y, test_x)
    create_submission(test_ids, output, 'neural_network')


def create_submission(test_ids, result, file_name):
    print("creating submission for " + file_name)
    output = pd.DataFrame(data={"id": test_ids, "sentiment": result})
    output.to_csv('bow-' + file_name + '.csv', index=False, quoting=3)
    print(file_name + "submission created successfully")
