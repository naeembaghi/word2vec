from sklearn.ensemble import RandomForestClassifier


def run(train_x, train_y, test_x):
    print("random forest process starting...")
    forest = RandomForestClassifier(n_estimators=100)
    forest = forest.fit(train_x, train_y)
    result = forest.predict(test_x)
    print("random forest process finished")
    return result
