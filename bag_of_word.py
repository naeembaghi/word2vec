from sklearn.feature_extraction.text import CountVectorizer
import read_data
import pandas as pd


def get():
    return normal_get()
    # return pre_loaded_get()


def pre_loaded_get():
    print("bag of word process starting...")
    train_y, test_data = read_data.pre_loaded_get()
    train_result = pd.read_csv("data/labeledTrainData.tsv", header=0, delimiter="\t", quoting=3)
    test_result = pd.read_csv("data/testData.tsv", header=0, delimiter="\t", quoting=3)
    print("bag of word process finished")
    return train_result, train_y, test_result, test_data



def normal_get():
    print("bag of word process starting...")
    train_x, train_y, test_x, test_data = read_data.read()
    vectorizer = CountVectorizer(analyzer="word",
                                 tokenizer=None,
                                 preprocessor=None,
                                 stop_words=None,
                                 max_features=5000)

    train_result = vectorizer.fit_transform(train_x)
    train_result.toarray()

    test_result = vectorizer.transform(test_x)
    test_result = test_result.toarray()
    print("bag of word process finished")
    return train_result, train_y, test_result, test_data["id"]
