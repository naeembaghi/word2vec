from sklearn.ensemble import AdaBoostClassifier


def run(train_x, train_y, test_x):
    print("ada boost process starting...")
    boost = AdaBoostClassifier(n_estimators=100)
    boost.fit(train_x, train_y)
    result = boost.predict(test_x)
    print("ada boost process finished")
    return result
