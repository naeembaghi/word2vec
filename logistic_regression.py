from sklearn.linear_model import LogisticRegression


def run(train_x, train_y, test_x):
    print("logistic regression process starting...")
    lr = LogisticRegression()
    lr.fit(train_x, train_y)
    result = lr.predict(test_x)
    print("logistic regression process finished")
    return result
