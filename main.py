import bag_of_word
import evaluator
# import bag_of_word
# import numpy as np
# import pandas as pd

# train_x, train_y, test_x, test_ids = bag_of_word.get()
# pd.DataFrame(np.asarray(train_x)).to_csv('train_bag_of_word.csv', header=None, index=None)
# pd.DataFrame(np.asarray(test_x)).to_csv('test_bag_of_word.csv', header=None, index=None)

train_x, train_y, test_x, test_ids = bag_of_word.get()
evaluator.logistic_regression(train_x, train_y, test_x, test_ids)
evaluator.ada_boost(train_x, train_y, test_x, test_ids)
evaluator.random_forest(train_x, train_y, test_x, test_ids)
evaluator.neural_network(train_x, train_y, test_x, test_ids)
evaluator.decision_tree(train_x, train_y, test_x, test_ids)
