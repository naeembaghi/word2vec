from sklearn.tree import DecisionTreeClassifier


def run(train_x, train_y, test_x):
    print("decision tree process starting...")
    tree = DecisionTreeClassifier()
    tree.fit(train_x, train_y)
    result = tree.predict(test_x)
    print("decision tree process finished")
    return result
