import pandas as pd
from bs4 import BeautifulSoup
import re
# from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

lemmatizer = WordNetLemmatizer()
# ps = PorterStemmer()


def read():
    train = pd.read_csv("data/labeledTrainData.tsv", header=0, delimiter="\t", quoting=3)
    test = pd.read_csv("data/testData.tsv", header=0, delimiter="\t", quoting=3)

    print("start cleaning train data...")
    num_reviews = train["review"].size
    train_reviews = []
    for i in range(0, num_reviews):
        # If the index is evenly divisible by 1000, print a message
        if (i + 1) % 1000 == 0:
            print("Review train %d of %d" % (i + 1, num_reviews))
        train_reviews.append(cleaner(train["review"][i]))

    train_result = []
    sentiment = train["sentiment"]
    for i in range(0, len(sentiment)):
        train_result.append(sentiment.T[i])
    print("cleaning train data finished")

    print("start cleaning test data...")
    num_reviews = test["review"].size
    test_reviews = []
    for i in range(0, num_reviews):
        # If the index is evenly divisible by 1000, print a message
        if (i + 1) % 1000 == 0:
            print("Review test %d of %d" % (i + 1, num_reviews))
        test_reviews.append(cleaner(test["review"][i]))
    print("cleaning test data finished")

    return train_reviews, train_result, test_reviews, test


# as lemmatizing data is to slow, I saved the data for next uses
# train_reviews.csv
# test_reviews.csv
def pre_loaded_get():
    train = pd.read_csv("data/labeledTrainData.tsv", header=0, delimiter="\t", quoting=3)
    test = pd.read_csv("data/testData.tsv", header=0, delimiter="\t", quoting=3)
    # train_reviews_file = pd.read_csv("train_reviews.csv", header=None, delimiter=",")
    # test_reviews_file = pd.read_csv("test_reviews.csv", header=None, delimiter=",")

    # num_reviews = len(train_reviews_file)
    # train_reviews = []
    # for i in range(0, num_reviews):
    #     train_reviews.append(train_reviews_file.T[i].all())
    #     num_reviews = len(test_reviews_file)

    # test_reviews = []
    # for i in range(0, num_reviews):
    #     test_reviews.append(test_reviews_file.T[i].all())

    train_result = []
    sentiment = train["sentiment"]
    for i in range(0, len(sentiment)):
        train_result.append(sentiment.T[i])

    return train_result, test


def cleaner(review):
    # Remove HTML
    BeautifulSoup(review, "html.parser").get_text()

    # Remove non-letters
    letters_only = re.sub("[^a-zA-Z]", " ", review)

    # Convert to lower case, split to individual words
    words = letters_only.lower().split()

    # Remove stop words
    words = [w for w in words if not w in stopwords.words("english")]

    # Stemming (seems lemmatizing is better than stemming
    # https://www.guru99.com/stemming-lemmatization-python-nltk.html)
    # words = list(map((lambda x: ps.stem(x)), words))

    # Lemmatizing
    # new_words = []
    # for num in range(0, len(words)):
    #     new_words.append(lemmatizer.lemmatize(words[num]))
    return " ".join(words)
